<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php print $title; ?></title>
  </head>
  <style>
    body{width:645px;margin: 0 auto;}
    table{border: 1px solid #000;border-collapse: collapse;border-spacing: 0;}
    td.col-title{background-color:#3366CC; text-align: center;color:#fff;font-weight: bold;}
    td{padding: 6px 10px;text-align: center;border: 1px solid #000;}
    tr>td:last-child{width:257px;text-align:center;}
    img.barcode{height: 55px;}
    ul{margin: 0;}
    h3{margin-bottom: 0;}
  </style>
  <body onload="confirmPrint()">
    <?php print $body; ?>
    <script>
      function confirmPrint() {
        if (confirm("是否列印條碼頁?")) {
          window.print();
        }
      }
    </script>
  </body>
</html>
