﻿<?php
//	儲存訂單的系統訊息
function uc_neweb_payment_save_comment($order_id, $message, $type = 'status'){
	uc_order_comment_save($order_id, 0, $message);
	if ('' != $type) {
		drupal_set_message($message, $type);
	}
	return $message;
}

//	確認訂單畫面
function uc_neweb_checkout_review(&$form, $min, $max, $hook){
	$order_id = intval($_SESSION['cart_order']);
	$order = uc_order_load($order_id);

  if ($order && $hook == $order->payment_method) {
    unset($form['submit']);
    if ($order->order_total <= $min) {
    	$message = t('Amount must be greater than @min_total.', array('@min_total' => $min));
			drupal_set_message($message, 'error');
			return;
		}

    if ($order->order_total >= $max) {
    	$message = t('Amount must be smaller than @max_total.', array('@max_total' => $max));
			drupal_set_message($message, 'error');
			return;
		}

    $form['#prefix'] = '<table id="neweb-credit-review-table"><tr><td>';
    $form['#suffix'] = '</td><td>'. drupal_get_form($hook . '_form', $order) .'</td></tr></table>';
  }

}
