﻿<?php

//	銀行交易回覆代碼
function neweb_code_BRC(){
	static $code = array();
	if (!empty($code)) {
		return $code;
	}

	$code['00'] = t('BRC: Approved or completed successfully ');
	$code['01'] = t('BRC: Refer to card issuer');
	$code['02'] = t('BRC: Refer to card issuer\'s special conditions');
	$code['03'] = t('BRC: Invalid merchant');
	$code['04'] = t('BRC: Pick-up card');
	$code['05'] = t('BRC: Do not honour');
	$code['06'] = t('BRC: Error');
	$code['07'] = t('BRC: Pick-up card, special condition');
//	$code['08'] = t('BRC: Honour with identification');
//	$code['11'] = t('BRC: Approved(VIP)');
	$code['12'] = t('BRC: Invalid transaction');
	$code['13'] = t('BRC: Invalid amount');
	$code['14'] = t('BRC: Invalid card number (no such number)');
	$code['15'] = t('BRC: No such issuer');
	$code['19'] = t('BRC: Re-Enter Transaction');
	$code['21'] = t('BRC: No Action Taken (Unable back out prior trans)');
	$code['25'] = t('BRC: Unable to Locate Record in File');
	$code['28'] = t('BRC: File Temporarily not Available for Update or Inquiry');
	$code['30'] = t('BRC: Format error');
//	$code['31'] = t('BRC: Bank not supported by switch');
	$code['33'] = t('BRC: Expired card');
//	$code['36'] = t('BRC: Restricted card');
//	$code['38'] = t('BRC: Allowable PIN tries exceeded');
	$code['41'] = t('BRC: Lost card');
	$code['43'] = t('BRC: Stolen card, pick-up');
	$code['51'] = t('BRC: Not sufficient funds');
	$code['54'] = t('BRC: Expired card');
	$code['55'] = t('BRC: Incorrect personal identification number (PIN)');
//	$code['56'] = t('BRC: No card record');
	$code['57'] = t('BRC: Transaction not permitted to cardholder');
	$code['61'] = t('BRC: Exceeds withdrawal amount limit');
	$code['62'] = t('BRC: Restricted card');
	$code['65'] = t('BRC: Exceeds withdrawal frequency limit');
//	$code['67'] = t('BRC: decline Exceeds withdrawal frequency limit Hart capture (requires that card be picked up at the ATM)');
//	$code['68'] = t('BRC: Response received too late');
	$code['75'] = t('BRC: Allowable number of PIN exceeded');
//	$code['76'] = t('BRC: Unable to Locate Previous Message');
	$code['80'] = t('BRC: Invalid Date');
	$code['81'] = t('BRC: Cryptographic Error Found in PIN or CVV');
	$code['82'] = t('BRC: Incorrect CVV');
	$code['85'] = t('BRC: No Reason To Decline a Request for AddressVerification');
//	$code['87'] = t('BRC: Bad track 2 (reserved for BASE24 use)');
//	$code['88'] = t('BRC: Reserved for private use');
//	$code['89'] = t('BRC: System error (reserved for BASE24 use)');
//	$code['90'] = t('BRC: Cutoff is in process (switch ending a day\'s business and starting the next. Transaction can be sent again in a few minutes)');
	$code['91'] = t('BRC: Issuer or switch is inoperative');
//	$code['92'] = t('BRC: Financial institution or intermediate  network facility cannot be found for routing');
	$code['93'] = t('BRC: Transaction cannot be Completed Violation of Law');
	$code['94'] = t('BRC: Duplicate transmission');
	$code['96'] = t('BRC: System malfunction');
	$code['99'] = t('BRC: Line Busy');
	$code['IE'] = t('BRC: ID Error');
	
	return $code;
}


//	主回覆碼
function neweb_code_PRC(){
	static $code = array();
	if (!empty($code)) {
		return $code;
	}

	$code['0'] = t('PRC: operation success');
	$code['1'] = t('PRC: operation pending');
	$code['2'] = t('PRC: undefined object');
	$code['3'] = t('PRC: parameter not found');
	$code['4'] = t('PRC: parameter too short');
	$code['5'] = t('PRC: parameter too long');
	$code['6'] = t('PRC: parameter format error');
	$code['7'] = t('PRC: parameter value error');
	$code['8'] = t('PRC: duplicate object');
	$code['9'] = t('PRC: parameter mismatch');
	$code['10'] = t('PRC: input error');
	$code['11'] = t('PRC: verb not valid in present state');
	$code['12'] = t('PRC: communication error');
	$code['13'] = t('PRC: internal etill error');
	$code['14'] = t('PRC: database error');
	$code['15'] = t('PRC: cassette error');
	$code['17'] = t('PRC: unsupported API version');
	$code['18'] = t('PRC: obsolete API version');
	$code['19'] = t('PRC: autoapprove failed');
	$code['20'] = t('PRC: autodeposit failed');
	$code['21'] = t('PRC: cassette not running');
	$code['22'] = t('PRC: cassette not valid');
	$code['23'] = t('PRC: unsupported in sysplex');
	$code['24'] = t('PRC: parameter null value');
	$code['30'] = t('PRC: XML error');
	$code['31'] = t('PRC: corequisite parameter not found');
	$code['32'] = t('PRC: invalid parameter combination');
	$code['33'] = t('PRC: batch error');
	$code['34'] = t('PRC: financial failure');
	$code['43'] = t('PRC: block black BIN');
	$code['44'] = t('PRC: block foreign');
	$code['50'] = t('PRC: servlet init error');
	$code['51'] = t('PRC: authentication error');
	$code['52'] = t('PRC: authorization error');
	$code['53'] = t('PRC: unhandled exception');
	$code['54'] = t('PRC: duplicate parameter value not allowed');
	$code['55'] = t('PRC: command not supported');
	$code['56'] = t('PRC: crypto error');
	$code['57'] = t('PRC: not active');
	$code['58'] = t('PRC: parameter not allowed');
	$code['59'] = t('PRC: delete error');
	$code['60'] = t('PRC: websphere');
	$code['61'] = t('PRC: supported in sysplex admin only');
	$code['62'] = t('PRC: realm');
	$code['32768'] = t('PRC: missing API version');
	$code['-1'] = t('PRC: dispathcer error');
	
	return $code;
}

